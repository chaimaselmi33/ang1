import { Component } from '@angular/core';
import { createWorker } from 'tesseract.js';
import {WebcamImage} from 'ngx-webcam';
import {Subject, Observable} from 'rxjs';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  // latest snapshot
  public webcamImage: WebcamImage = null;
  // webcam snapshot trigger
  private trigger: Subject<void> = new Subject<void>();
  triggerSnapshot(): void {
   this.trigger.next();
  }

  handleImage(webcamImage: WebcamImage): void {
   console.info('received webcam image', webcamImage);
   this.webcamImage = webcamImage;
   console.log(this.webcamImage)
   this.doOCR(this.webcamImage.imageAsDataUrl)
  }

  public get triggerObservable(): Observable<void> {
   return this.trigger.asObservable();
  }


 title = 'ang1';
  ocrResult = 'Recognizing...';
  constructor() {
    //this.doOCR();
  }
  async doOCR(img) {
    const worker = createWorker({
      logger: m => console.log(m),
    });
    await worker.load();
    await worker.loadLanguage('eng');
    await worker.initialize('eng');
    const { data: { text } } = await worker.recognize(img);
    this.ocrResult = text;
    console.log(text);
    await worker.terminate();
  }
}
